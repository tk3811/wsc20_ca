#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  6 19:25:16 2020

@author: tk381
"""

import glob
import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

working_dir = "/home/tk381/homeoffice/wsc_rust_ca_projects/"

runs = glob.glob(working_dir + "optimal_baseline/target/criterion/*memory*")

res = {}
for r in runs:
    name = int(r.split("/")[-1].split("_")[-1])
    res[name] = {}
    for sub_dir in glob.glob(r + "/*"):
        if not "report" in sub_dir:
            data = json.load(open(sub_dir + "/new/estimates.json"))
            sub_num = sub_dir.split("/")[-1]
            res[name][sub_num] = int(data["Mean"]["point_estimate"])

plt.gcf().set_size_inches(9, 2.7)

gs = gridspec.GridSpec(ncols=10, nrows=1, figure=plt.gcf())
plt.subplot(gs[0, 0:5])
for i in sorted(res.keys()):
    x_vals_int = list(map(int, sorted(res[i].keys())))
    x_vals_int.sort()
    x_vals_int = np.array(x_vals_int)
    x_vals_str = list(map(str, x_vals_int))
    # print(res[i].values())
    y_vals = list(map(lambda x: res[i][x], x_vals_str))

    y_vals = np.array(list(map(int, y_vals)))

    plt.semilogx(x_vals_int, x_vals_int / y_vals * 1000, label=str(int(i / 8 + 1)) + ' bytes')

plt.xlim(right=1e10)
plt.ylim(bottom=140)
plt.legend(loc='lower right')

plt.xlabel('System size [elements]')
plt.ylabel('Melem/s')

plt.subplot(gs[0, 5:])
plt.vlines(10485760,200,800,linestyles='--')
for i in sorted(res.keys()):
    x_vals_int = list(map(int, sorted(res[i].keys())))
    x_vals_int.sort()
    x_vals_int = np.array(x_vals_int)
    x_vals_str = list(map(str, x_vals_int))
    # print(res[i].values())
    y_vals = list(map(lambda x: res[i][x], x_vals_str))

    y_vals = np.array(list(map(int, y_vals)))

    plt.semilogx(x_vals_int * (i / 8 + 1), x_vals_int / y_vals * 1000, label=str(int(i / 8 + 1)) + ' bytes')
plt.gca().set_yticklabels([])
plt.xlabel('System size [bytes]')
# plt.ylabel('Melem/s')
plt.ylim(bottom=140)

#plt.legend()
plt.tight_layout()
plt.savefig('memory3.pdf')
# print(runs)
