import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from execute_run import execute_run

wdir = '~/work_mosi/wsc_rust_ca/'

reps = 50
sizes = [20,40,80,120,160,200,250,300,350,400,500,600,700,850,1000]
sizes = [200,350,600,1200]

max_size = sizes[-1]
if False:
    dats = []
    for s in sizes:
        dats.append(execute_run(True,True,"matrix_add",s,reps))#*int(max_size*max_size/(s*s))))

    df_tot = pd.DataFrame()
    df_tot = df_tot.append(dats,ignore_index=True)

    df_tot.to_csv('scaling_output.csv')
    #df_tot = pd.read_csv(wdir + 'output_3.csv')

df_tot = pd.read_csv('scaling_output.csv')

df_tot["rngs"]= df_tot["Healthy"]+df_tot["Dead"]

df = df_tot


prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']

#fig2 = plt.figure(constrained_layout=True)
plt.gcf().set_size_inches(9,4)
#gs = gridspec.GridSpec(ncols=1, nrows=len(sizes), figure=fig2)


for i in range(len(sizes)):
    df = df_tot[df_tot.Total == (sizes[i]-2)*(sizes[i]-2)]
    stepmean = df.groupby("Step").mean()
    plt.subplot(2,2,i+1)
    num_reps = df.Rep.max()
    #plt.title('Model behavior at n = '+str(sizes[i]))
    plt.plot([],label='n = '+str(sizes[i]),alpha=0)
    if i != 2 and i != 3:
        plt.gca().set_xticklabels([])
    else:
        plt.xlabel('Steps')
    for rn in range(num_reps+1):
        if rn != 0:
            plt.plot(df.Step[df.Rep == rn],df[df.Rep == rn]["Healthy"]/df[df.Rep == rn]["Total"]*100,c=colors[1],alpha=.2)
        else:
            plt.plot(df.Step[df.Rep == rn], df[df.Rep == rn]["Healthy"] / df[df.Rep == rn]["Total"] * 100, c=colors[1],
                     alpha=.3, label='Single run')
    #plt.scatter(df.Step,df["Healthy"]/df["Total"]*100,label='n = '+str(sizes[i]),c='y',s=.1,alpha=.3)
    plt.plot(stepmean.Healthy/stepmean["Total"]*100,label='Average',c=colors[0])##,label='Healty ave')
    #plt.plot(df.Step,df["rngs"]/df["Total"]*100,label='Healty + Dead')
    #plt.plot(df.Step,df["Ordered"] / df["Total"] * 100, label='orderd')
    if i == 0 or i == 2:
        plt.ylabel("% healthy")
    else:
        plt.gca().set_yticklabels([])
    plt.legend(loc='upper right')

#plt.gcf().subplots_adjust(hspace=.1,wspace=.1)
plt.tight_layout()
plt.savefig('plot_scaling_multi_panel.pdf',dpi=400)


plt.clf()

plt.gcf().set_size_inches(10,5)
#gs = gridspec.GridSpec(ncols=1, nrows=6, figure=fig2)



for i in range(len(sizes)):
    df = df_tot[df_tot.Total == (sizes[i]-2)*(sizes[i]-2)]
    stepmean = df_tot.groupby("Step").mean()
    num_reps = df.Rep.max()
    #plt.title('Model behavior at n = '+str(sizes[i]))
    for rn in range(num_reps+1):
        plt.plot(df.Step[df.Rep == rn],df[df.Rep == rn]["Healthy"]/df[df.Rep == rn]["Total"]*100,c=colors[i],alpha=.3)#,marker='8',label='Healty')#,s=1)#,alpha=1)
    #plt.plot(stepmean.Healthy/stepmean["Total"]*100,label='Healty ave')
    #plt.plot(df.Step,df["rngs"]/df["Total"]*100,label='Healty + Dead')
    #plt.plot(df.Step,df["Ordered"] / df["Total"] * 100, label='orderd')
    plt.ylabel("%")
    #plt.legend(loc='upper right')



plt.tight_layout()
plt.savefig('plot_scaling_single_panel.png',dpi=800)
#plt.show()


