#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  1 16:28:17 2020

@author: till
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from execute_run import execute_run

wdir = '~/work_mosi/wsc_rust_ca/'

size = 300
reps = 1


if False:
    df1 = execute_run(False,False,"base",size,reps)#pd.read_csv(wdir + 'output_v_s.csv')
    df2 = execute_run(False,True,"base",size,reps)#pd.read_csv(wdir + 'output_v_f.csv')

    df1b = execute_run(True,False,"base",size,reps)#pd.read_csv(wdir + 'output_v_s.csv')
    df2b = execute_run(True,True,"base",size,reps)#pd.read_csv(wdir + 'output_v_f.csv')

    df3 = execute_run(False,False,"matrix_add",size,reps)#pd.read_csv(wdir + 'output_t_s.csv')
    df4 = execute_run(False,True,"matrix_add",size,reps)

    df3b = execute_run(True,False,"matrix_add",size,reps)#pd.read_csv(wdir + 'output_t_s.csv')
    df4b = execute_run(True,True,"matrix_add",size,reps)

    df5 = execute_run(False,True,"vectorized",size,reps)
    df5b = execute_run(True,True,"vectorized",size,reps)

    df_tot = df1.append([df1b,df2,df2b,df3,df3b,df4,df4b,df5,df5b],ignore_index=True)

    df_tot.to_csv('total_output.csv')
    #df_tot = pd.read_csv(wdir + 'output_3.csv')

df_tot = pd.read_csv('total_output.csv')

df_tot["rngs"]= df_tot["Healthy"]+df_tot["Dead"]

stepmean = df_tot.groupby("Step").mean()


fig2 = plt.figure(constrained_layout=True)
plt.gcf().set_size_inches(9,6.1)
gs = gridspec.GridSpec(ncols=1, nrows=6, figure=fig2)

fig2.add_subplot(gs[0,0])
#plt.title('Model behavior')
#plt.title('Ordered Cells')
plt.text(200,75,'a) Cell counts',ha="center",bbox=dict(boxstyle="square",
                   ec='black',#(1., 0.5, 0.5),
                   fc='white'#(1., 0.8, 0.8),
                   ))
plt.plot(stepmean["rngs"]/stepmean["Total"]*100,label='Healthy + dead')
plt.plot(stepmean["Healthy"]/stepmean["Total"]*100,label='Healthy')
plt.ylabel("%")
plt.gca().set_xticklabels([])
plt.legend(loc='upper right')


#plt.subplot(3,1,2)
fig2.add_subplot(gs[1,0])
#plt.title('Ordered Cells')
plt.text(200,83,'b) Ordered cells',ha="center",bbox=dict(boxstyle="square",
                   ec='black',#(1., 0.5, 0.5),
                   fc='white'#(1., 0.8, 0.8),
                   ))
plt.plot(stepmean["Ordered"]/stepmean["Total"]*100,label='orderd cells')
plt.ylabel("%")
#plt.legend(loc='upper right')
plt.gca().set_xticklabels([])
#plt.xlabel('Step')


#plt.subplot(3,1,3)
fig2.add_subplot(gs[2:4,0])
#plt.title('Throughput w/ good random numbers')
plt.text(200,380,'c) Throughput w/ high quality random numbers',ha="center",bbox=dict(boxstyle="square",
                   ec='black',#(1., 0.5, 0.5),
                   fc='white'#(1., 0.8, 0.8),
                   ))
n_tot = df_tot.Total.mean()
plt.ylabel('Melem/s')
#plt.ylim(bottom=0)

plt.plot(n_tot/df_tot[(df_tot.sim == 'vectorized') & (df_tot.xoro == False) ].groupby("Step").mean()["Steptime"]/1e6,label='Delayed random calculation')
plt.plot(n_tot/df_tot[(df_tot.sim == 'matrix_add') & (df_tot.xoro == False) & (df_tot.rng_opt)].groupby("Step").mean()["Steptime"]/1e6,label='Matrix add + reduced random numbers')
plt.plot(n_tot/df_tot[(df_tot.sim == 'matrix_add') & (df_tot.xoro == False) & (~df_tot.rng_opt)].groupby("Step").mean()["Steptime"]/1e6,label='Matrix add')
plt.plot(n_tot/df_tot[(df_tot.sim == 'base') & (df_tot.xoro == False) & (df_tot.rng_opt)].groupby("Step").mean()["Steptime"]/1e6,label='Baseline + reduced random numbers')
plt.plot(n_tot/df_tot[(df_tot.sim == 'base') & (df_tot.xoro == False) & (~df_tot.rng_opt)].groupby("Step").mean()["Steptime"]/1e6,label='Baseline')

plt.legend(loc='upper right')
plt.gca().set_xticklabels([])

#plt.subplot(3,1,3)
fig2.add_subplot(gs[4:,0])
plt.ylabel('Melem/s')
#plt.title('Throughput w/ fast random numbers')
plt.text(200,400,'d) Throughput w/ fast random numbers',ha="center",bbox=dict(boxstyle="square",
                   ec='black',#(1., 0.5, 0.5),
                   fc='white'#(1., 0.8, 0.8),
                   ))
n_tot = df_tot.Total.mean()

plt.plot(n_tot/df_tot[(df_tot.sim == 'vectorized') & (df_tot.xoro == True) ].groupby("Step").mean()["Steptime"]/1e6,label='Delayed random calculation')
plt.plot(n_tot/df_tot[(df_tot.sim == 'matrix_add') & (df_tot.xoro == True) & (df_tot.rng_opt)].groupby("Step").mean()["Steptime"]/1e6,label='Matrix add + reduced random numbers')
plt.plot(n_tot/df_tot[(df_tot.sim == 'matrix_add') & (df_tot.xoro == True) & (~df_tot.rng_opt)].groupby("Step").mean()["Steptime"]/1e6,label='Matrix add')
plt.plot(n_tot/df_tot[(df_tot.sim == 'base') & (df_tot.xoro == True) & (df_tot.rng_opt)].groupby("Step").mean()["Steptime"]/1e6,label='Baseline + reduced random numbers')
plt.plot(n_tot/df_tot[(df_tot.sim == 'base') & (df_tot.xoro == True) & (~df_tot.rng_opt)].groupby("Step").mean()["Steptime"]/1e6,label='Baseline')

plt.legend(loc='upper right')
plt.xlabel('Steps')

#plt.plot(n_tot/df_tot[(df_tot.sim == 'base') & (df_tot.xoro == False)].groupby("Step").mean()["Steptime"]/1e6,label='base + reduced random numbers')
#plt.plot(n_tot/df_tot[(df_tot.sim == 'base') & (df_tot.xoro == False)].groupby("Step").mean()["Steptime"]/1e6,label='base + reduced random numbers')


plt.savefig('plot.pdf')
#plt.show()


plt.clf()

res = df_tot.groupby(["xoro","sim","rng_opt","Rep"]).mean()

res["thrput"] = res.Total/res.Steptime/1e6
res["setup"] = res.Env / res.Steptime *100


res_mean = res.mean(level=["xoro","sim","rng_opt"])
res_std = res.sem(level=["xoro","sim","rng_opt"])
res_mean["thrput2"] = (res_mean.thrput).apply(lambda v: "${:.1f}".format(v)) + (res_std.thrput).apply(lambda v: "\pm{:.2f}$".format(v))
res_mean["setup2"] = (res_mean.setup).apply(lambda v: "${:.1f}".format(v)) + (res_std.setup).apply(lambda v: "\pm{:.2f}$".format(v))

print(res_mean.to_latex(columns=["thrput2","setup2"],escape=False))
"""
plt.plot(df2.Steptime)
plt.plot(df2.Env)

print("Total throuput v s: " + str(df1.Total[0]*len(df1)/df1.Steptime.sum()*1e-6)+ "/" +str(df1.Total[0]*len(df1)/(df1.Steptime.sum()-df1.Env.sum())*1e-6))
print("Total throuput v f: " + str(df2.Total[0]*len(df2)/df2.Steptime.sum()*1e-6))
print("Total throuput t s: " + str(df3.Total[0]*len(df3)/df3.Steptime.sum()*1e-6))
print("Total throuput t f: " + str(df4.Total[0]*len(df4)/df4.Steptime.sum()*1e-6))
print("Total throuputALL: " + str(df_tot.Total[0]*len(df_tot)/df_tot.Steptime.sum()*1e-6))

"""
