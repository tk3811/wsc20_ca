import os
import pandas as pd
from pathlib import Path

def execute_run(xoro: bool, rng_opt: bool, simulator, size: int, reps: int, P_HIV: float = 0.05):
    assert (simulator in ['base', 'matrix_add', 'vectorized'])
    assert (size > 4)

    working_dir = str(Path(__file__).resolve().parent.parent) + "/wsc_rust_ca/"
    print(working_dir)
    filename = working_dir + "src/main.rs"
    with open(filename, 'r') as file:
        filedata = file.readlines()

    outfile = ""
    for l in filedata:
        if "let t_env = /*auto gen step*/" in l:
            if simulator == "base":
                outfile += "let t_env = /*auto gen step*/ sim.step(&mut rng);"
            elif simulator == "matrix_add":
                outfile += "let t_env = /*auto gen step*/ sim.step_matrix_add(&mut rng);"
            elif simulator == "vectorized":
                outfile += "let t_env = /*auto gen step*/ sim.step_full_vec(&mut rng);"
            else:
                raise Exception("unknown option")
        elif "const P_HIV: f64 = /*automatic generation */" in l:
            print(l)
            #raise Exception("test")
            outfile += "const P_HIV: f64 = /*automatic generation */ "+ str(P_HIV) +" as f64; // Fraction (probability) of cells initially infected by virus\n"
        elif "return /*auto generated, what rng*/" in l:
            if rng_opt:
                outfile += "return /*auto generated, what rng*/ self.sample_fast(rng);"
            else:
                outfile += "return /*auto generated, what rng*/ self.sample_slow(rng);"
        elif "let mut sim = CA_system::new(/* auto generated*/" in l:
            outfile += "let mut sim = CA_system::new(/* auto generated*/" + str(size) + ");"
        elif "let num_reps = /*auto generated*/" in l:
            outfile += "let num_reps = /*auto generated*/ " + str(reps) + ";"
        elif "let mut rng = /*auto generated*/" in l:
            if (xoro):
                outfile += "let mut rng = /*auto generated*/ rand_xoshiro::Xoroshiro128Plus::from_entropy();"
            else:
                outfile += "let mut rng = /*auto generated*/ thread_rng();"

        else:
            outfile += l

    # Replace the target string
    # filedata = filedata.replace('ram', 'abcd')

    # Write the file out again
    with open(filename, 'w') as file:
        file.write(outfile)

    os.system("rustfmt " + filename)
    try:
        os.remove('output.csv')
    except:
        pass
    os.system("cargo run --manifest-path=" + working_dir + "Cargo.toml --release")
    df = pd.read_csv('output.csv')
    df["xoro"] = xoro
    df["rng_opt"] = rng_opt
    df["sim"] = simulator
    df["P_HIV"] = P_HIV
    return df

print(execute_run(False,False, "base",10,3))
# execute_run(True,True, "matrix_add",100,30)
# execute_run(False,True, "vectorized",100,30)
