#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  1 16:28:17 2020

@author: till
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from execute_run import execute_run

wdir = '~/work_mosi/wsc_rust_ca/'

size = 1200
reps = 100


param_vals = [0,0.05,0.3,0.95]

if False:
    dfs = []
    for p in param_vals:
        dfs.append( execute_run(False,False,"matrix_add",size,reps,p))#pd.read_csv(wdir + 'output_t_s.csv')

    df = pd.DataFrame()

    df_tot = df.append(dfs,ignore_index=True)

    df_tot.to_csv('param_scan.csv')
    #df_tot = pd.read_csv(wdir + 'output_3.csv')

df_tot = pd.read_csv('param_scan.csv')

#df_tot["rngs"]= df_tot["Healthy"]+df_tot["Dead"]

stepmean = df_tot.groupby("Step").mean()


prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']


fig2 = plt.figure(constrained_layout=True)
plt.gcf().set_size_inches(8,2)
gs = gridspec.GridSpec(ncols=1, nrows=6, figure=fig2)

for i in range(len(param_vals)):
    df = df_tot[df_tot.P_HIV == param_vals[i]]
    stepmean = df.groupby("Step").mean()
    plt.subplot(1,4,i+1)
    num_reps = df.Rep.max()
    #plt.title('Model behavior at n = '+str(sizes[i]))
    plt.plot([],label='P_HIV = '+str(param_vals[i]),alpha=0)
    plt.xlim(-20,400)
    plt.title('P_HIV = '+str(param_vals[i]))
    if i != 0:
        #plt.gca().set_xticklabels([])
        plt.gca().set_yticklabels([])

    plt.xlabel('Steps')

    #for rn in range(num_reps+1):
    #    if rn != 0:
    #        plt.plot(df.Step[df.Rep == rn],df[df.Rep == rn]["Healthy"]/df[df.Rep == rn]["Total"]*100,c=colors[1],alpha=.2)
    #    else:
    #        plt.plot(df.Step[df.Rep == rn], df[df.Rep == rn]["Healthy"] / df[df.Rep == rn]["Total"] * 100, c=colors[1],
    #                 alpha=.3, label='single run')

    #plt.scatter(df.Step,df["Healthy"]/df["Total"]*100,label='n = '+str(sizes[i]),c='y',s=.1,alpha=.3)
    plt.plot(stepmean.Healthy/stepmean["Total"]*100,c=colors[0])##,label='Healty ave')
    plt.ylim(bottom=-2)
    #plt.plot(df.Step,df["rngs"]/df["Total"]*100,label='Healty + Dead')
    #plt.plot(df.Step,df["Ordered"] / df["Total"] * 100, label='orderd')
    if i == 0:# or i == 2:
        plt.ylabel("% healthy")
    #plt.legend(loc='upper right')

plt.gcf().subplots_adjust(hspace=.1,wspace=.13)
plt.tight_layout()
plt.savefig('plot_param_multi_panel.pdf',dpi=400)