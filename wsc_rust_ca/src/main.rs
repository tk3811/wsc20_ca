use core::cmp;
use ndarray::*;
use rand::prelude::*;
use rand::rngs::SmallRng;
use statrs::distribution::Geometric;
use std::time::Instant;
use std::{thread, time};
//use rand_xoshiro::Xoroshiro128PlusPlus;
use std::fs::File;
use std::io::prelude::*;

//Parameters
const P_HIV: f64 = /*automatic generation */ 0.05 as f64; // Fraction (probability) of cells initially infected by virus
const P_I: f64 = 0.997; // Probability of healthy cell becoming infected by neighbouring cells
const P_V: f64 = 0.00001; // Probability of healthy cell becoming infected by a random virus from outside neighbourhood
const P_REP: f64 = 0.99; // Probability of a dead cell becoming replaced by a healthy cell
const P_REPI: f64 = 0.00001; // Probability of a replaced dead cell of becoming infected

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
enum State {
    Healthy = 0,
    A1_0 = 17,
    A1_1 = 18,
    A1_2 = 19,
    A1_3 = 20,
    A2 = 2,
    Dead = 1,
}

#[derive(Clone)]
struct gridState {
    edge_length: usize,
    dat: ndarray::Array2<State>,
}

impl gridState {
    fn new(l: usize) -> gridState {
        gridState {
            edge_length: l,
            dat: ndarray::Array2::<State>::from_shape_vec((l, l), vec![State::Healthy; l * l])
                .unwrap(),
        }
    }

    fn neighbors_infected_xy(&self, x: usize, y: usize) -> bool {
        let x = x as isize;
        let y = y as isize;
        /*let xmin = cmp::max(x - 1, 0) as usize;
        let xmax = cmp::min(x + 1, (DIM - 1) as isize) as usize;

        let ymin = cmp::max(y - 1, 0) as usize;
        let ymax = cmp::min(y + 1, (DIM - 1) as isize) as usize;*/

        let mut count = 0;

        //let the_slice = self.dat.slice(ndarray::s![x - 1..=x + 1, y - 1..=y + 1]).map(|v|*v as u8).sum();
        /*return the_slice.iter().filter(|v| **v == State::A2).count() >= 4
        || the_slice.iter().any(|k| match k {
            State::Healthy | State::Dead | State::A2 => false,
            _ => true,
        });*/

        /*return self
        .dat
        .slice(ndarray::s![x - 1..=x + 1, y - 1..=y + 1])
        .iter()
        .any(|v| match *v {
            State::Healthy | State::Dead => false,
            State::A2 => {
                count += 1;
                if count == 4 {
                    true
                } else {
                    false
                }
            }
            _ => true,
        });*/

        let mut sum = 0;
        unsafe {
            for ix in (x - 1)..=(x + 1) {
                for iy in (y - 1)..=(y + 1) {
                    sum += *self.dat.uget((ix as usize, iy as usize)) as u8;
                }
            }
        }

        return sum >= State::A1_0 as u8;

        // BRANCHING BASED
        unsafe {
            for ix in (x - 1)..=(x + 1) {
                for iy in (y - 1)..=(y + 1) {
                    match self.dat.uget((ix as usize, iy as usize)) {
                        //[[ix as usize,iy as usize]] {
                        State::Healthy | State::Dead => (),
                        State::A2 => {
                            count += 1;
                            if count == 4 {
                                return true;
                            }
                        }
                        _ => return true,
                    }
                }
            }
            false
        }
    }
}

pub struct binomial_generator {
    p: f64,
    counter: usize,
}
impl binomial_generator {
    pub fn sample_fast<T: Rng>(&mut self, rng: &mut T) -> bool {
        if self.counter == 0 {
            let distro = Geometric::new(self.p).unwrap();
            let r = distro.sample(rng);
            self.counter = r as usize - 1;
            return true;
        }
        self.counter -= 1;
        return false;
    }

    pub fn sample_default<T: Rng>(&mut self, rng: &mut T) -> bool {
        return /*auto generated, what rng*/ self.sample_slow(rng);
    }

    pub fn sample_slow<T: Rng>(&self, rng: &mut T) -> bool {
        rng.gen_bool(self.p)
    }

    pub fn new(p: f64) -> binomial_generator {
        assert!(p < 0.1);
        let mut res = binomial_generator { p, counter: 0 };
        res.sample_fast(&mut thread_rng());
        res
    }
}

fn step_transform<T: Rng>(input: &gridState, output: &mut gridState, rng: &mut T) -> (f64) {
    let mut neg_gen_p_i = binomial_generator::new(1.0 - P_I);
    let mut gen_p_v = binomial_generator::new(P_V);
    let mut neg_gen_p_rep = binomial_generator::new(1.0 - P_REP);
    let mut gen_p_repi = binomial_generator::new(P_REPI);

    assert!(input.edge_length % 2 == 0);
    let red_length = (input.edge_length - 2) / 2;
    let mut reduced_grid = ndarray::Array2::from_shape_vec(
        (red_length, red_length),
        vec![false; red_length * red_length],
    )
    .unwrap();
    ndarray::Zip::from(&mut reduced_grid)
        .and(input.dat.slice(ndarray::s![1..-2;2, 1..-2;2]))
        .and(input.dat.slice(ndarray::s![1..-2;2, 2..-1;2]))
        .and(input.dat.slice(ndarray::s![2..-1;2, 1..-2;2]))
        .and(input.dat.slice(ndarray::s![2..-1;2, 2..-1;2]))
        .apply(|mut t, o1, o2, o3, o4| {
            *t = ((*o4 as u32 + *o1 as u32 + *o2 as u32 + *o3 as u32) >= 9)
        });
    println!(
        "We have {}/{}",
        reduced_grid.iter().filter(|v| **v).count(),
        reduced_grid.iter().count()
    );
    /*let mut countA = 0;
    let mut countB = 0;
    let mut countC = 0;*/
    ndarray::Zip::indexed(&mut output.dat.slice_mut(ndarray::s![1..-1, 1..-1]))
        .and(&input.dat.slice(ndarray::s![1..-1, 1..-1]))
        .apply(|(x, y), out, inp| {
            //println!(" {} {}",x,y);
            *out = match inp {
                State::Healthy => {
                    match  /*unsafe{*reduced_grid.uget((x/2,y/2)) }  ||*/ (input.neighbors_infected_xy(x + 1, y + 1) && !neg_gen_p_i.sample_default(rng)) || gen_p_v.sample_default(rng) {
                                true => {
                                    State::A1_0
                                }
                                false => State::Healthy,
                            }
                        }
                State::A1_0 => State::A1_1,
                State::A1_1 => State::A1_2,
                State::A1_2 => State::A1_3,
                State::A1_3 => {
                    State::A2
                }
                State::A2 => {

                    State::Dead
                }
                State::Dead => match !neg_gen_p_rep.sample_default(rng) {
                    true => {
                        match gen_p_repi.sample_default(rng){
                            true => State::A1_0,
                            false => State::Healthy
                        }
                    }
                    false => State::Dead,
                },
            }
        });

    return 0.0;
}
fn step_transform_2<T: Rng>(
    //input: &gridState,
    output: &mut gridState,
    rng: &mut T,
) -> f64 {
    let mut neg_gen_p_i = binomial_generator::new(1.0 - P_I);
    let mut gen_p_v = binomial_generator::new(P_V);
    let mut neg_gen_p_rep = binomial_generator::new(1.0 - P_REP);
    let mut gen_p_repi = binomial_generator::new(P_REPI);

    let s = Instant::now();
    let reduced_grid =
        ndarray::Array2::<u8>::zeros((output.edge_length - 2, output.edge_length - 2))
            + output
                .dat
                .slice(ndarray::s![0..-2, 0..-2])
                .map(|v| *v as u8)
            + output
                .dat
                .slice(ndarray::s![0..-2, 1..-1])
                .map(|v| *v as u8)
            + output.dat.slice(ndarray::s![0..-2, 2..]).map(|v| *v as u8)
            + output
                .dat
                .slice(ndarray::s![1..-1, 0..-2])
                .map(|v| *v as u8)
            + output.dat.slice(ndarray::s![1..-1, 2..]).map(|v| *v as u8)
            + output.dat.slice(ndarray::s![2.., 0..-2]).map(|v| *v as u8)
            + output.dat.slice(ndarray::s![2.., 1..-1]).map(|v| *v as u8)
            + output.dat.slice(ndarray::s![2.., 2..]).map(|v| *v as u8);

    /*let the_windows = input
    .dat
    .windows((3, 3));*/

    /* let reduced_grid_vec: Vec<u8> = input
        .dat
        .windows((3, 3))
        .into_iter()
        .map(|v| v.map(|st| *st as u8).sum())
        .collect();
    let reduced_grid = ndarray::Array2::from_shape_vec(
        (input.edge_length - 2, input.edge_length - 2),
        reduced_grid_vec,
    )
    .unwrap();*/
    let env_time = (Instant::now() - s).as_secs_f64();

    //println!("{:?} != {:?}",reduced_grid,reduced_grid_2);
    //assert!(reduced_grid == reduced_grid_2);
    println!("# env in {}s", env_time);

    /* println!("We have {}/{}",reduced_grid.iter().filter(|v|**v).count(),reduced_grid.iter().count());
    let mut countA = 0;
    let mut countB = 0;
    let mut countC = 0;*/

    ndarray::Zip::from(&mut output.dat.slice_mut(ndarray::s![1..-1, 1..-1]))
        //.and(&input.dat.slice(ndarray::s![1..-1, 1..-1]))
        .and(&reduced_grid)
        //.and(the_windows)
        .apply(|out, /*inp,*/ envo| {
            //println!(" {} {}",x,y);
            *out = match *out {
                State::Healthy => {
                    match *envo /*.map(|v|*v as u8).sum()*/ >= State::A1_0 as u8 {
                        //(unsafe{*reduced_grid.uget((x/2,y/2)) } || input.neighbors_infected_xy(x + 1, y + 1)) {
                        //rng.gen_bool(0.2) {
                        false => {
                            //countA += 1;
                            match gen_p_v.sample_default(rng) {
                                true => State::A1_0,
                                false => State::Healthy,
                            }
                        }
                        true => {
                            match !neg_gen_p_i.sample_default(rng) {
                                true => {
                                    //ob.count_healthy -= 1;
                                    State::A1_0
                                }
                                false => State::Healthy,
                            }
                        }
                    }
                }
                State::A1_0 => State::A1_1,
                State::A1_1 => State::A1_2,
                State::A1_2 => State::A1_3,
                State::A1_3 => State::A2,
                State::A2 => State::Dead,
                State::Dead => match !neg_gen_p_rep.sample_default(rng) {
                    true => match gen_p_repi.sample_default(rng) {
                        true => State::A1_0,
                        false => State::Healthy,
                    },
                    false => State::Dead,
                },
            }
        });

    return env_time;
}

fn step_transform_full_vec<T: Rng>(
    //input: &gridState,
    output: &mut gridState,
    rng: &mut T,
) -> f64 {
    let mut neg_gen_p_i = binomial_generator::new(1.0 - P_I);
    let mut gen_p_v = binomial_generator::new(P_V);
    let mut neg_gen_p_rep = binomial_generator::new(1.0 - P_REP);
    let mut gen_p_repi = binomial_generator::new(P_REPI);

    let s = Instant::now();
    let mut reduced_grid =
        ndarray::Array2::<u8>::zeros((output.edge_length - 2, output.edge_length - 2))
            + output
                .dat
                .slice(ndarray::s![0..-2, 0..-2])
                .map(|v| *v as u8)
            + output
                .dat
                .slice(ndarray::s![0..-2, 1..-1])
                .map(|v| *v as u8)
            + output.dat.slice(ndarray::s![0..-2, 2..]).map(|v| *v as u8)
            + output
                .dat
                .slice(ndarray::s![1..-1, 0..-2])
                .map(|v| *v as u8)
            + output.dat.slice(ndarray::s![1..-1, 2..]).map(|v| *v as u8)
            + output.dat.slice(ndarray::s![2.., 0..-2]).map(|v| *v as u8)
            + output.dat.slice(ndarray::s![2.., 1..-1]).map(|v| *v as u8)
            + output.dat.slice(ndarray::s![2.., 2..]).map(|v| *v as u8);

    {
        let num_not_happening = rand_distr::Binomial::new(
            ((output.edge_length - 2) * (output.edge_length - 2)) as u64,
            1.0 - P_I,
        )
        .unwrap()
        .sample(rng);
        println!("NOT HAPPENING: {}", num_not_happening);
        (0..num_not_happening).for_each(|_| {
            reduced_grid[[
                rng.gen_range(0, output.edge_length - 2),
                rng.gen_range(0, output.edge_length - 2),
            ]] = 0
        });
    }

    {
        let num_extra_happening = rand_distr::Binomial::new(
            ((output.edge_length - 2) * (output.edge_length - 2)) as u64,
            P_V,
        )
        .unwrap()
        .sample(rng);
        println!("Extra HAPPENING: {}", num_extra_happening);
        (0..num_extra_happening).for_each(|_| {
            reduced_grid[[
                rng.gen_range(0, output.edge_length - 2),
                rng.gen_range(0, output.edge_length - 2),
            ]] = State::A1_0 as u8
        });
    }

    {
        let num_not_happening = rand_distr::Binomial::new(
            ((output.edge_length - 2) * (output.edge_length - 2)) as u64,
            1.0 - P_REP,
        )
        .unwrap()
        .sample(rng);
        println!("NOT HAPPENING DEAD: {}", num_not_happening);
        (0..num_not_happening).for_each(|_| {
            let (mx, my) = (
                rng.gen_range(0, output.edge_length - 2),
                rng.gen_range(0, output.edge_length - 2),
            );
            if output.dat[[mx, my]] == State::Dead {
                output.dat[[mx, my]] = State::A2;
            }
        });
    }

    let env_time = (Instant::now() - s).as_secs_f64();

    println!("# env in {}s", env_time);

    ndarray::Zip::from(&mut output.dat.slice_mut(ndarray::s![1..-1, 1..-1]))
        //.and(&input.dat.slice(ndarray::s![1..-1, 1..-1]))
        .and(&reduced_grid)
        //.and(the_windows)
        .apply(|out, /*inp,*/ envo| {
            //println!(" {} {}",x,y);
            *out = match *out {
                State::Healthy => {
                    match *envo /*.map(|v|*v as u8).sum()*/ >= State::A1_0 as u8 {
                        false => State::Healthy,
                        true => State::A1_0,
                    }
                }
                State::A1_0 => State::A1_1,
                State::A1_1 => State::A1_2,
                State::A1_2 => State::A1_3,
                State::A1_3 => State::A2,
                State::A2 => State::Dead,
                State::Dead => State::Healthy, /*match !neg_gen_p_rep.sample_default(rng) {
                                                   true => match gen_p_repi.sample_default(rng) {
                                                       true => State::A1_0,
                                                       false => State::Healthy,
                                                   },
                                                   false => State::Dead,
                                               },*/
            }
        });

    return env_time;
}

struct CA_system {
    cells_a: gridState,
    cells_b: gridState,

    a_is_current: bool,

    rng: rand_xoshiro::Xoroshiro128PlusPlus, //SmallRng,

    l: usize,
    //count_healthy: isize,
    //count_dead: isize,
    //count_a2: isize,
}

impl CA_system {
    fn get_current(&self) -> &gridState {
        match self.a_is_current {
            true => &self.cells_a,
            false => &self.cells_b,
        }
    }

    pub fn new(l: usize) -> CA_system {
        let mut r = CA_system {
            cells_a: gridState::new(l),

            rng: rand_xoshiro::Xoroshiro128PlusPlus::from_entropy(), //SmallRng::from_entropy(),
            cells_b: gridState::new(l),
            a_is_current: true,
            l,
        };

        for x in (1..l - 1) {
            for y in (1..l - 1) {
                r.cells_a.dat[[x, y]] = {
                    match r.rng.gen_bool(P_HIV) {
                        true => State::A1_0,
                        false => State::Healthy,
                    }
                };
            }
        }
        //println!("{:?}",r.cells_a.dat);
        r.cells_b = r.cells_a.clone();
        //println!("{:?}",r.cells_b.dat);
        //println!("INITIAL: {} {:?}",r.count_healthy,r.get_observables());

        return r;
    }

    pub fn get_observables_names() -> Vec<String> {
        vec!["Total", "Healthy", "A1*", "A2", "Dead", "Ordered"]
            .into_iter()
            .map(|v| String::from(v))
            .collect()
    }

    pub fn get_observables(&self) -> Vec<usize> {
        let mut result = vec![0; CA_system::get_observables_names().len()];
        let mut last = State::Healthy;
        result[0] = self
            .get_current()
            .dat
            .slice(ndarray::s![1..-1, 1..-1])
            .iter()
            .count();
        self.get_current()
            .dat
            .slice(ndarray::s![1..-1, 1..-1])
            .iter()
            .for_each(|v| {
                if *v == last {
                    result[5] += 1
                } else {
                    last = *v
                }
            });
        self.get_current()
            .dat
            .slice(ndarray::s![1..-1, 1..-1])
            .iter()
            .for_each(|s: &State| match *s {
                State::Healthy => result[1] += 1,
                State::A1_0 | State::A1_1 | State::A1_2 | State::A1_3 => result[2] += 1,
                State::A2 => result[3] += 1,
                State::Dead => result[4] += 1,
            });

        assert!(result.len() == CA_system::get_observables_names().len());
        result
    }

    pub fn print(&self) {
        //assert!(self.l < 1000);

        for x in 1..self.l - 1 {
            for y in 1..self.l - 1 {
                match self.get_current().dat[[x, y]] {
                    State::Healthy => print!("H{} ", {
                        match self.get_current().neighbors_infected_xy(x, y) {
                            true => "*",
                            false => " ",
                        }
                    }),
                    State::A1_0 => print!("1  "),
                    State::A1_1 => print!("2  "),
                    State::A1_2 => print!("3  "),
                    State::A1_3 => print!("4  "),
                    State::A2 => print!("A  "),
                    State::Dead => print!("D  "),
                }
            }
            println!("");
        }
        println!("");
    }

    pub fn step<T: Rng>(&mut self, rng: &mut T) -> f64 {
        self.a_is_current = !self.a_is_current;
        let t = match self.a_is_current {
            false => step_transform(&self.cells_a, &mut self.cells_b, rng),
            true => step_transform(&self.cells_b, &mut self.cells_a, rng),
        };
        return t;
    }
    pub fn step_matrix_add<T: Rng>(&mut self, rng: &mut T) -> f64 {
        //self.a_is_current = !self.a_is_current;
        assert!(self.a_is_current);
        let t = step_transform_2(&mut self.cells_a, rng);
        ///*&mut self.cells_b, rng), //match self.a_is_current {
        //   false => step_transform_2(&self.cells_a, /*&mut self.cells_b, rng),
        //     true => step_transform_2(&self.cells_b, &mut self.cells_a, rng),
        //};*/
        return t;
    }
    pub fn step_full_vec<T: Rng>(&mut self, rng: &mut T) -> f64 {
        //self.a_is_current = !self.a_is_current;
        assert!(self.a_is_current);
        let t = step_transform_full_vec(&mut self.cells_a, rng);
        ///*&mut self.cells_b, rng), //match self.a_is_current {
        //   false => step_transform_2(&self.cells_a, /*&mut self.cells_b, rng),
        //     true => step_transform_2(&self.cells_b, &mut self.cells_a, rng),
        //};*/
        return t;
    }
}

pub fn main_test_rng() {
    let max = 1;
    let mut rng = rand_xoshiro::Xoroshiro128Plus::from_entropy();
    let test_vals = vec![0.00001, 0.1, 0.5, 0.9, 0.999999];
    for p in test_vals {
        let mut d = binomial_generator::new(p);
        println!(
            "{} -> {}",
            p,
            (0..max).filter(|_| d.sample_fast(&mut rng)).count() as f64 / max as f64
        );
    }
}

fn main() {
    let num_reps = /*auto generated*/ 3;
    let mut file = File::create("output.csv").unwrap();
    CA_system::get_observables_names()
        .iter()
        .for_each(|v| write!(file, "{},", v).unwrap());
    writeln!(file, "Steptime,Step,Env,Rep").unwrap();
    for rep in 0..num_reps {
        let total_start = Instant::now();
        let mut rng = /*auto generated*/ thread_rng();
        let mut sim = CA_system::new(/* auto generated*/ 10);
        for i in 0..600 {
            sim.get_observables()
                .into_iter()
                .for_each(|o| write!(file, "{},", o).unwrap());

            //sim.print();
            //thread::sleep(time::Duration::from_millis(1000));

            let s = Instant::now();
            let t_env = /*auto gen step*/ sim.step(&mut rng);
            let steptime = (Instant::now() - s).as_secs_f32();
            writeln!(file, "{},{},{},{}", steptime, i, t_env, rep).unwrap();
            println!("# {}Step in {}s", i, steptime);
        }

        println!(
            "# # TOTAL TIME {}s",
            (Instant::now() - total_start).as_secs_f32()
        );
    }
}
