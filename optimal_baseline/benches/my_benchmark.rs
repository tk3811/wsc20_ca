use criterion::BenchmarkId;
use criterion::Throughput;
use criterion::{black_box, criterion_group, criterion_main, Criterion};
use optimal_baseline::*;
use rand::{thread_rng, Rng, SeedableRng};
use rand_xoshiro::Xoshiro256Plus;
use std::mem::swap;

const MAX_POW: u32 = 5;

fn get_bench_sizes() -> Vec<usize> {
    vec![1e8 as usize]
    //get_req_sizes().into_iter().map(|v| v * v).collect()
    //vec![400*400]
    //(0..22).map(|n| (2 as usize).pow(n)).collect()
    //vec![10000, 100000, 1000000, 4000000]
    //vec![10000]
}

fn range_elem(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut group = c.benchmark_group("theoretical_limit");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data = get_random_states(*size);
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.iter_mut()
                    .for_each(|val| *val = next_state_simple(val))
            })
        });
    }
    group.finish();
}

fn range_elem_size8(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut group = c.benchmark_group("theoretical_limit_memory_8");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data: Vec<(u8, State)> = get_random_states(*size)
            .iter()
            .map(|v| (thread_rng().gen_range(0, std::u8::MAX), *v))
            .collect();
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.iter_mut().for_each(|(counter, val)| {
                    *val = next_state_simple(val);
                    *counter += 1;
                })
            })
        });
    }
    group.finish();
}

fn range_elem_size16(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut group = c.benchmark_group("theoretical_limit_memory_16");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data: Vec<(u16, State)> = get_random_states(*size)
            .iter()
            .map(|v| (thread_rng().gen_range(0, std::u16::MAX), *v))
            .collect();
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.iter_mut().for_each(|(counter, val)| {
                    *val = next_state_simple(val);
                    *counter += 1;
                })
            })
        });
    }
    group.finish();
}

fn range_elem_size32(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut group = c.benchmark_group("theoretical_limit_memory_32");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data: Vec<(u32, State)> = get_random_states(*size)
            .iter()
            .map(|v| (thread_rng().gen_range(0, std::u32::MAX), *v))
            .collect();
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.iter_mut().for_each(|(counter, val)| {
                    *val = next_state_simple(val);
                    *counter += 1;
                })
            })
        });
    }
    group.finish();
}

fn range_elem_size64(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut group = c.benchmark_group("theoretical_limit_memory_64");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data: Vec<(u64, State)> = get_random_states(*size)
            .iter()
            .map(|v| (thread_rng().gen_range(0, std::u64::MAX), *v))
            .collect();
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.iter_mut().for_each(|(counter, val)| {
                    *val = next_state_simple(val);
                    *counter += 1;
                })
            })
        });
    }
    group.finish();
}

fn range_elem_size128(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut group = c.benchmark_group("theoretical_limit_memory_128");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data: Vec<(u128, State)> = get_random_states(*size)
            .iter()
            .map(|v| (thread_rng().gen_range(0, std::u128::MAX), *v))
            .collect();
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.iter_mut().for_each(|(counter, val)| {
                    *val = next_state_simple(val);
                    *counter += 1;
                })
            })
        });
    }
    group.finish();
}

fn range_elem_size256(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut group = c.benchmark_group("theoretical_limit_memory_256");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data: Vec<(u128,u128, State)> = get_random_states(*size)
            .iter()
            .map(|v| (thread_rng().gen_range(0, std::u128::MAX), thread_rng().gen_range(0, std::u128::MAX), *v))
            .collect();
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.iter_mut().for_each(|(counter1,counter2, val)| {
                    *val = next_state_simple(val);
                    *counter1 += 1;
                    *counter2 -= 1;
                })
            })
        });
    }
    group.finish();
}

fn range_elem_buffer(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut group = c.benchmark_group("theoretical_limit_buffer");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data1 = get_random_states(*size);
        let mut data2 = data1.iter().copied().collect();
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data2 = data1
                    .iter()
                    .map(|val| return next_state_simple(val))
                    .collect();
                swap(&mut data1, &mut data2)
            })
        });
    }
    group.finish();
}

fn range_elem_non_vec(c: &mut Criterion) {
    let mut rng = Xoshiro256Plus::from_entropy();//thread_rng();

    let sizes = get_bench_sizes();
    let mut group = c.benchmark_group("theoretical_limit_no_vec");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data = get_random_states(*size);
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.iter_mut()
                    .for_each(|val| *val = next_state_simple_non_vec(val,&mut rng))
            })
        });
    }
    group.finish();
}

fn range_elem_nd_masked(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut rng = Xoshiro256Plus::from_entropy();//thread_rng();
    let potential_states = [
        State::A,
        State::B,
        State::C,
        State::D,
        State::E,
        State::F,
        State::G,
        State::H,
        State::J,
        State::K,
    ];
    let mut group = c.benchmark_group("limit_nd_masked");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data1 = ndarray::Array1::from( get_random_states(*size));
        let mut data2 = data1.clone();
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data2.fill(State::A);
                for s in potential_states.iter() {
                    (0..(*size / (potential_states.len()*30))).for_each(|_| data2[rng.gen_range(0, *size)] = *s);
                }
                ndarray::Zip::from(&mut data1).and(&data2).apply(|mut a,b|*a = next_state_masked(a,b));
            })
        });
    }
    group.finish();
}

fn range_elem_nd_masked2(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut rng = Xoshiro256Plus::from_entropy();//thread_rng();
    let potential_states = [
        State::A,
        State::B,
        State::C,
        State::D,
        State::E,
        State::F,
        State::G,
        State::H,
        State::J,
        State::K,
    ];
    let mut group = c.benchmark_group("limit_nd_masked2");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data1 = ndarray::Array1::from( get_random_states(*size));
        let mut data2 = data1.clone();
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data2.fill(State::A);
                for s in potential_states.iter() {
                    (0..(*size / (potential_states.len()*30))).for_each(|_| data2[rng.gen_range(0, *size)] = *s);
                }
                ndarray::Zip::from(&mut data1).and(&data2).apply(|mut a,b|*a = next_state_masked(a,b));
            })
        });
    }
    group.finish();
}

fn range_elem_non_vec_nd_buff(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut rng = Xoshiro256Plus::from_entropy();//thread_rng();

    let mut group = c.benchmark_group("theoretical_limit_no_vec_nd_buff");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data1 = ndarray::Array1::from( get_random_states(*size));
        let mut data2 = data1.clone();
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                ndarray::Zip::from(&mut data1).and(&data2).apply(|mut a,b|*a = next_state_simple_non_vec(b,&mut rng));
            })
        });
    }
    group.finish();
}

fn range_elem_non_vec_nd_buff_uniform(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut rng = Xoshiro256Plus::from_entropy();//thread_rng();

    let mut group = c.benchmark_group("theoretical_limit_no_vec_nd_buff_uniformdata");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data1 = ndarray::Array1::from( vec![State::F;*size]);//get_random_states(*size));
        let mut data2 = data1.clone();
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                ndarray::Zip::from(&mut data1).and(&data2).apply(|mut a,b|*a = next_state_simple_non_vec(b,&mut rng));
            })
        });
    }
    group.finish();
}

fn range_elem_non_vec_nd(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut rng = thread_rng();

    let mut group = c.benchmark_group("theoretical_limit_no_vec_nd");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data = ndarray::Array1::from(get_random_states(*size));
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.iter_mut().for_each(|val| *val = next_state_simple_non_vec(val,&mut rng));
            })
        });
    }
    group.finish();
}

fn range_elem_non_vec_rect_filter(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut rng = thread_rng();

    let mut group = c.benchmark_group("theoretical_limit_no_vec_rect_filter");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data = get_random_states(*size);
        let (x, y) = get_xy_decomposition(*size);
        println!("{} * {} = {}", x, y, size);
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.iter_mut()
                    .enumerate()
                    .filter(|(n, v)| n % y != 0 && n % y != y - 1 && *n > x && *n < x * y - x)
                    .map(|(n, v)| v)
                    .for_each(|val| *val = next_state_simple_non_vec(val,&mut rng));
            })
        });
    }
    group.finish();
}

fn range_elem_non_vec_rect(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut rng = thread_rng();

    let mut group = c.benchmark_group("theoretical_limit_no_vec_rect_chunked");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut data = get_random_states(*size);
        let (x, y) = get_xy_decomposition(*size);
        println!("{} * {} = {}", x, y, size);
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.chunks_mut(y).for_each(|chunk| {
                    chunk
                        .iter_mut()
                        .skip(1)
                        .take(y - 2)
                        .for_each(|val| *val = next_state_simple_non_vec(val,&mut rng))
                });
            })
        });
    }
    group.finish();
}

fn range_elem_random(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut group = c.benchmark_group("theoretical_limit_random");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut rng = thread_rng();
        let mut data = get_random_states(*size);
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.iter_mut()
                    .for_each(|val| *val = random_next_state(val, &mut rng))
            })
        });
    }
    group.finish();
}

fn range_elem_random_xo(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut group = c.benchmark_group("theoretical_limit_random_xoro");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut rng = Xoshiro256Plus::from_entropy();
        let mut data = get_random_states(*size);
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data.iter_mut()
                    .for_each(|val| *val = random_next_state(val, &mut rng))
            })
        });
    }
    group.finish();
}

fn range_elem_random_xo_buffer(c: &mut Criterion) {
    let sizes = get_bench_sizes();
    let mut group = c.benchmark_group("theoretical_limit_random_xoro_buffer");
    for size in sizes.iter() {
        group.throughput(Throughput::Elements(*size as u64));
        let mut rng = Xoshiro256Plus::from_entropy();
        let mut data1 = get_random_states(*size);
        let mut data2 = data1.iter().copied().collect();
        group.bench_function(BenchmarkId::from_parameter(size), |b| {
            b.iter(|| {
                data2 = data1
                    .iter()
                    .map(|val| return random_next_state(val, &mut rng))
                    .collect();
                swap(&mut data1, &mut data2)
            })
        });
    }
    group.finish();
}

fn get_req_sizes() -> Vec<usize> {

    let mut res : Vec<usize> = vec![];//1,2,3,4,5,6,7,8];

    let mut c :usize = 1;
    while c*c < 3e7 as usize {
        res.push(c);
        c *= 2;
    }
    return res;

    //vec![200]
    (1..30).map(|v|v*v).collect()
    //vec![10000, 100000, 1000000, 4000000]
    //vec![10000]
}

fn req_non_nd(c: &mut Criterion) {
    let sizes = get_req_sizes();
    let mut rng = thread_rng();

    let mut group = c.benchmark_group("req_noVec_iter2_nd");
    for size in sizes.iter() {
        let size2: u64 = (*size * *size) as u64;

        println!("SIZES {} and {}", size, size2);
        group.throughput(Throughput::Elements(size2));
        let mut data1 = ndReqtangle::new(*size);//Reqtangle::new(*size);
        let mut data2 = ndReqtangle::new(*size);//Reqtangle::new(*size);
        group.bench_function(BenchmarkId::from_parameter(size2), |b| {
            b.iter(|| {
                data1
                    .iter_center()
                    .zip(data2.iter_center_mut())
                    .for_each(|(vin, vout)| *vout = next_state_simple_non_vec(vin,&mut rng));
                swap(&mut data1, &mut data2);
            })
        });
    }
    group.finish();
}

fn req_non_vec(c: &mut Criterion) {
    let mut rng = thread_rng();

    let sizes = get_req_sizes();
    let mut group = c.benchmark_group("req_noVec_iter2");
    for size in sizes.iter() {
        let size2: u64 = (*size * *size) as u64;

        println!("SIZES {} and {}", size, size2);
        group.throughput(Throughput::Elements(size2));
        let mut data1 = Reqtangle::new(*size);
        let mut data2 = Reqtangle::new(*size);
        group.bench_function(BenchmarkId::from_parameter(size2), |b| {
            b.iter(|| {
                data1
                    .iter_center()
                    .zip(data2.iter_center_mut())
                    .for_each(|(vin, vout)| *vout = next_state_simple_non_vec(vin,&mut rng));
                swap(&mut data1, &mut data2);
            })
        });
    }
    group.finish();
}

fn req_non_vec_dummy_zip(c: &mut Criterion) {
    let sizes = get_req_sizes();
    let mut rng = thread_rng();

    let mut group = c.benchmark_group("req_noVec_dummyZip");
    for size in sizes.iter() {
        let size2: u64 = (*size * *size) as u64;

        group.throughput(Throughput::Elements(size2));
        let mut data1 = Reqtangle::new(*size);
        let mut data2 = Reqtangle::new(*size);
        group.bench_function(BenchmarkId::from_parameter(size2), |b| {
            b.iter(|| {
                data1
                    .iter_center_env()
                    .zip(data2.iter_center_mut())
                    .for_each(|((in1,in2,in3,in4,in5,in6,in7,in8), vout)| *vout = next_state_simple_non_vec(in3,&mut rng));
                swap(&mut data1, &mut data2);
            })
        });
    }
    group.finish();
}

fn req_non_vec_env(c: &mut Criterion) {
    let sizes = get_req_sizes();
    let mut group = c.benchmark_group("req_noVec_env");
    for size in sizes.iter() {
        let size2: u64 = (*size * *size) as u64;

        group.throughput(Throughput::Elements(size2));
        let mut data1 = ndReqtangle::new(*size);
        let mut data2 = ndReqtangle::new(*size);
        group.bench_function(BenchmarkId::from_parameter(size2), |b| {
            b.iter(|| {
                /*data1
                    .iter_center_env()
                    .zip(data2.iter_center_mut())
                    .for_each(|((in1,in2,in3,in4,in5,in6,in7,in8), vout)| *vout = next_state_simple_non_vec(in3));*/
                data1.apply_testing2(&data2);
                swap(&mut data1, &mut data2);
            })
        });
    }
    group.finish();
}

fn req_non_vec_full(c: &mut Criterion) {
    let mut rng = thread_rng();

    let sizes = get_req_sizes();
    let mut group = c.benchmark_group("req_noVec_fulliter");
    for size in sizes.iter() {
        let size2: u64 = (*size * *size) as u64;

        group.throughput(Throughput::Elements(size2));
        let mut data1 = Reqtangle::new(*size);
        let mut data2 = Reqtangle::new(*size);
        group.bench_function(BenchmarkId::from_parameter(size2), |b| {
            b.iter(|| {
                data1
                    .data
                    .iter()
                    .zip(data2.data.iter_mut())
                    .for_each(|(vin, vout)| *vout = next_state_simple_non_vec(vin,&mut rng));
                swap(&mut data1, &mut data2);
            })
        });
    }
    group.finish();
}

/*criterion_group!(
    size_benches,
    range_elem_nd_masked,
    range_elem_nd_masked2
);
criterion_main!(size_benches);
*/


//criterion_group!(benches, range_elem,range_elem_buffer, range_elem_non_vec, range_elem_random,range_elem_random_xo,range_elem_random_xo_buffer);
//criterion_main!(benches);

criterion_group!(benches, range_elem,range_elem_buffer,range_elem_non_vec_nd,range_elem_non_vec_nd_buff,range_elem_non_vec_nd_buff_uniform,range_elem_non_vec, range_elem_random,range_elem_random_xo,range_elem_random_xo_buffer);
criterion_main!(benches);

/*criterion_group!(
    size_benches,
    range_elem_size8,
    range_elem_size16,
    range_elem_size32,
    range_elem_size64,
    range_elem_size128,
    range_elem_size256
);
criterion_main!(size_benches);
*/
/*criterion_group!(
    neighbor_benches,
    range_elem_non_vec,
    range_elem_non_vec_rect_filter,
    range_elem_non_vec_rect
);
criterion_main!(neighbor_benches);*/

/*criterion_group!(
    neighbor_benches,
    req_non_nd,
    req_non_vec,
    req_non_vec_env,
    /*req_non_vec_dummy_zip,
    range_elem_non_vec_rect_filter,
    range_elem_non_vec,
    range_elem_non_vec_rect,
    req_non_vec_full*/
);
criterion_main!(neighbor_benches);*/
