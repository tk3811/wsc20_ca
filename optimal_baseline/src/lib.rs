use rand::seq::SliceRandom;
use rand::{thread_rng, Rng};
//use rand::prelude::ThreadRng;
use itertools::izip;
use itertools::Itertools;
use ndarray::*;

pub fn get_xy_decomposition(n: usize) -> (usize, usize) {
    let max: usize = n;
    let mut best = (0, 0);
    let mut trial = 4;
    while trial * trial < n {
        let other: usize = n / trial;
        if other * trial == n {
            best = (trial, other);
        }
        trial += 1;
    }
    assert!(best != (0, 0));
    best
}

pub struct Reqtangle {
    pub data: Vec<State>,
    l: usize,
}

impl Reqtangle {
    pub fn new(l: usize) -> Reqtangle {
        Reqtangle {
            data: get_random_states(l * l),
            l,
        }
    }
    /* pub fn iter_center(&self) -> impl Iterator<Item = &State> {
        let my_l = self.l;
        self.data.chunks(self.l)
            .skip(1)
            .take(my_l - 2)
            .map(|i| i.iter().skip(1).take(my_l - 2))
            .flatten()
    }*/

    pub fn iter_stencil(&self) -> impl Iterator<Item = (&State, &State)> {
        izip!(self.iter_center(), self.iter_center())
    }
}

impl<'a> Reqtangle {
    pub fn iter_center(&'a self) -> impl Iterator<Item = &State> + 'a {
        self.data
            .chunks(self.l)
            .skip(1)
            .take(self.l - 2)
            .map(move |chunk| chunk.iter().skip(1).take(self.l - 2))
            .flatten()
    }

    pub fn iter_center_mut(&mut self) -> impl Iterator<Item = &mut State> {
        self.data
            .chunks_mut(self.l)
            .skip(1)
            .take(self.l - 2)
            .map(move |chunk| chunk.iter_mut().skip(1))
            .flatten()
        //let my_l = self.l;
        /*self.data.chunks_mut(self.l)
        .skip(1)
        .take(self.l - 2)
        .map( move|chunk | chunk.iter_mut().skip(1).take(self.l - 2))
        .flatten()*/
    }

    pub fn iter_center_env(
        &'a self,
    ) -> impl Iterator<
        Item = (
            &State,
            &State,
            &State,
            &State,
            &State,
            &State,
            &State,
            &State,
        ),
    > + 'a {
        izip!(
            self.data
                .chunks(self.l)
                .skip(0) //
                .take(self.l - 2)
                .map(move |chunk| chunk.iter().skip(0).take(self.l - 2))
                .flatten(),
            self.data
                .chunks(self.l)
                .skip(0) //
                .take(self.l - 2)
                .map(move |chunk| chunk.iter().skip(1).take(self.l - 2))
                .flatten(),
            self.data
                .chunks(self.l)
                .skip(0) //
                .take(self.l - 2)
                .map(move |chunk| chunk.iter().skip(2).take(self.l - 2))
                .flatten(),
            // skip 1
            self.data
                .chunks(self.l)
                .skip(1) //
                .take(self.l - 2)
                .map(move |chunk| chunk.iter().skip(0).take(self.l - 2))
                .flatten(),
            self.data
                .chunks(self.l)
                .skip(1) //
                .take(self.l - 2)
                .map(move |chunk| chunk.iter().skip(2).take(self.l - 2))
                .flatten(),
            // skip 2
            self.data
                .chunks(self.l)
                .skip(2) //
                .take(self.l - 2)
                .map(move |chunk| chunk.iter().skip(0).take(self.l - 2))
                .flatten(),
            self.data
                .chunks(self.l)
                .skip(2) //
                .take(self.l - 2)
                .map(move |chunk| chunk.iter().skip(1).take(self.l - 2))
                .flatten(),
            self.data
                .chunks(self.l)
                .skip(2) //
                .take(self.l - 2)
                .map(move |chunk| chunk.iter().skip(2).take(self.l - 2))
                .flatten()
        )
    }
}

pub struct ndReqtangle {
    pub data: ndarray::Array2<State>,
    l: usize,
}

impl ndReqtangle {
    pub fn new(l: usize) -> ndReqtangle {
        ndReqtangle {
            data: ndarray::Array2::<State>::from_shape_vec((l, l), get_random_states(l * l))
                .unwrap(),
            l,
        }
    }
    pub fn iter_all(&self) -> impl Iterator<Item = &State> {
        self.data.iter()
    }
    pub fn iter_all_mut(&mut self) -> impl Iterator<Item = &mut State> {
        self.data.iter_mut()
    }
    pub fn iter_center(&self) -> impl Iterator<Item = &State> {
        self.data.slice(ndarray::s![1..-1, 1..-1]).into_iter()
    }
    pub fn iter_center_mut(&mut self) -> impl Iterator<Item = &mut State> {
        self.data.slice_mut(ndarray::s![1..-1, 1..-1]).into_iter()
    }

    pub fn apply_testing(&mut self, other: &ndReqtangle) {
        let mut count = 0;
        let mut rng = thread_rng();
        ndarray::Zip::from(&mut self.data.slice_mut(ndarray::s![1..-1, 1..-1]))
            .and(&other.data.slice(ndarray::s![0..-2, 0..-2]))
            .and(&other.data.slice(ndarray::s![0..-2, 1..-1]))
            .and(&other.data.slice(ndarray::s![0..-2, 2..]))
            .and(&other.data.slice(ndarray::s![1..-1, 0..-2]))
            //.and(&other.data.slice(ndarray::s![1..-1, 2..]))
            //.and(&other.data.slice(ndarray::s![2.., 0..-2]))
            //.and(&other.data.slice(ndarray::s![2.., 1..-1]))
            .and(&other.data.slice(ndarray::s![2.., 2..]))
            .apply(|target, &n1, &n2, &n3, &n4, &n5 /*,&n6,&n7,&n8*/| {
                *target = next_state_simple_non_vec(&n3, &mut rng);
                count += 1
            });
        assert_eq!(count, (self.l - 2) * (self.l - 2));
    }

    pub fn apply_testing2(&mut self, other: &ndReqtangle) {
        let mut count = 0;
        self.data
            .slice_mut(ndarray::s![1..-1, 1..-1])
            .indexed_iter_mut()
            .for_each(|((nx, ny), value)| {
                *value = other.data[[nx + 1, ny + 1]];
                count += 1;
            });
        assert_eq!(count, (self.l - 2) * (self.l - 2));
    }

    /*pub fn iter_center_env(& self) -> ndarray::Zip {//impl Iterator<Item = (&State,&State)> {
        let y = ndarray::Zip::from(self.data.slice(ndarray::s![1..-1,1..-1])).and(&self.data.slice(ndarray::s![1..-1,1..-1]));
        y
    }*/
}

#[derive(Copy, Clone)]
pub enum State {
    C,
    F,
    G,
    A,
    H,
    E,
    J,
    D,
    B,
    K,
    Never,
}

pub fn next_state_simple(input: &State) -> State {
    match *input {
        State::A => State::B,
        State::C => State::D,
        State::F => State::G,
        State::G => State::H,
        State::J => State::K,
        State::H => State::J,
        State::B => State::C,
        State::K => State::A,
        State::D => State::E,
        State::E => State::F,
        State::Never => State::Never,
    }
}

pub fn random_next_state<T: Rng>(input: &State, rng: &mut T) -> State {
    match input {
        State::A | State::C | State::J | State::F => next_state_simple(input),
        _ => match rng.gen_bool(0.43) {
            true => match *input {
                State::A => State::B,
                State::C => State::D,
                State::F => State::G,
                State::G => State::H,
                State::H => State::J,
                State::J => State::K,

                State::B => State::C,
                State::K => State::A,
                State::D => State::E,
                State::E => State::F,
                State::Never => State::Never,
            },
            false => match *input {
                State::A => State::F,
                State::C => State::E,
                State::F => State::A,
                State::G => State::C,
                State::H => State::J,
                State::J => State::K,
                State::B => State::H,
                State::K => State::G,
                State::D => State::D,
                State::E => State::B,
                State::Never => State::Never,
            },
        },
    }
}

pub fn next_state_masked(input: &State, mask: &State) -> State {
    match mask {
        State::A | State::C  | State::F => next_state_simple(input),
        State::B | State::E => match *input {
            State::A => State::A,
            State::C => State::C,
            State::F => State::F,
            State::G => State::G,
            State::H => State::H,
            State::J => State::J,
            State::B => State::B,
            State::K => State::K,
            State::D => State::D,
            State::E => State::E,
            State::Never => State::Never,
        },
        _ => match *input {
            State::A => State::F,
            State::C => State::E,
            State::F => State::A,
            State::G => State::C,
            State::H => State::J,
            State::J => State::K,
            State::B => State::H,
            State::K => State::G,
            State::D => State::D,
            State::E => State::B,
            State::Never => State::Never,
        },
    }
}

pub fn next_state_simple_non_vec<T: Rng>(input: &State, rng: &mut T) -> State {
    //return random_next_state(input,rng);
    match *input {
        State::A => State::B,
        State::C => State::D,
        State::F => State::G,
        State::G => State::H,
        State::J => State::K,
        State::H => State::J,
        State::B => State::C,
        State::K => State::A,
        State::D => State::E,
        State::E => State::F,
        State::Never => {
            random_next_state(input, rng)
            /*let potential_states = [
                State::A,
                State::B,
                State::C,
                State::D,
                State::E,
                State::F,
                State::G,
                State::H,
                State::I,
                State::J,
                State::K,
            ];
            //let mut rng = thread_rng();
            *potential_states.choose( rng).unwrap()*/
        }
    }
}

pub fn get_random_states(n: usize) -> Vec<State> {
    let potential_states = [
        State::A,
        State::B,
        State::C,
        State::D,
        State::E,
        State::F,
        State::G,
        State::H,
        State::J,
        State::K,
    ];
    let mut rng = thread_rng();
    (0..n)
        .map(|_| *potential_states.choose(&mut rng).unwrap())
        .collect()
}

pub fn fibonacci(n: u64) -> u64 {
    match n {
        0 => 1,
        1 => 1,
        n => fibonacci(n - 1) + fibonacci(n - 2),
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
